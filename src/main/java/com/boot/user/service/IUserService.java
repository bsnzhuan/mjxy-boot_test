package com.boot.user.service;

import com.boot.user.entity.User;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2019-03-18
 */
public interface IUserService extends IService<User> {
	User selectUserByUsername(String userName);
}
