package com.boot.user.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.boot.user.entity.User;
import com.boot.user.mapper.UserMapper;
import com.boot.user.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2019-03-18
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectUserByUsername(String userName) {
        Wrapper<User> wrapper = new EntityWrapper<>();
        wrapper.eq(User.USERNAME,userName);
        List<User> users = userMapper.selectList(wrapper);

        return users.get(0);
    }
}
