package com.boot.user.web;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.boot.user.entity.User;
import com.boot.user.service.IUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2019-03-18
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService iUserService;

    @RequestMapping("/getuser")
    public String getUser(Integer id){
        return iUserService.selectById(id).toString();
    }

    @RequestMapping("/deleteuser")
    public Object deleteUser(Integer id){
        return iUserService.deleteById(id);
    }

    @RequestMapping("/selectpage")
    public Object selectPage(Integer pageNum,Integer pageSize){
        Wrapper<User> wrapper = new EntityWrapper<>();
        Page<User> page = new Page<>((pageNum-1)*pageSize,pageSize);
        return iUserService.selectPage(page).getRecords();
    }
    @RequestMapping("/login")
    public String login(){
        return "loginPage";
    }

    @RequestMapping("/loginAction")
    public String loginAction(String userName){
        Subject subject = SecurityUtils.getSubject();
        AuthenticationToken token = new UsernamePasswordToken(userName,"");
        try {
            subject.login(token);
            return "login Success!";
        }catch (Exception e){
           // throw
            return "login Failed!";
        }
    }
}
