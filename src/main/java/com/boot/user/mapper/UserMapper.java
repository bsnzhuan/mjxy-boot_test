package com.boot.user.mapper;

import com.boot.user.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-03-18
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}