package com.boot.role.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2019-03-24
 */
@Controller
@RequestMapping("/role")
public class RoleController {
	
}
