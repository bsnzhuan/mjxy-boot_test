package com.boot.role.mapper;

import com.boot.role.entity.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2019-03-24
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}