package com.boot.role.service;

import com.boot.role.entity.Role;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2019-03-24
 */
public interface IRoleService extends IService<Role> {
	
}
