package com.xy.test;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor //带参构造方法
public class User {
    private String name;
    private String sex;
}
